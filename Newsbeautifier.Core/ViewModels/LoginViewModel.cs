namespace NewsBeautifier.Core.ViewModels
{
	using System;
	using System.Threading.Tasks;
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using Microsoft.WindowsAzure.MobileServices;
	using NewsBeautifier.Core.Settings;
	using Newtonsoft.Json.Linq;
	using Windows.UI.Popups;

	public class LoginViewModel : MvxViewModel
	{
		ICommand _facebookLoginCommand;

		public LoginViewModel()
		{
			InitializeCommands();
		}
		public ICommand FacebookLoginCommand
		{
			get { return _facebookLoginCommand; }
		}

		private void InitializeCommands()
		{
			_facebookLoginCommand = new MvxCommand(async () => await SignInFacebook());
		}

		private async Task SignInFacebook()
		{
			string error = string.Empty;

			try
			{
				JObject token = new JObject();
				token.Add("access_token", "89ac69b9746697db770e1f771366b312");
				MobileServiceClient client = CommonAppSettings.MobileService;

				await client.LoginAsync(MobileServiceAuthenticationProvider.Facebook, token); // App Storage
				ShowViewModel<MainViewModel>();
			}
			catch (InvalidOperationException ioe)
			{
				error = ioe.Message;
			}
			if (!string.IsNullOrEmpty(error))
			{
				var dialog = new MessageDialog(error, "Erreur lors du Login");
				dialog.Commands.Add(new UICommand("Ok"));
				await dialog.ShowAsync();
			}
		}
	}
}
