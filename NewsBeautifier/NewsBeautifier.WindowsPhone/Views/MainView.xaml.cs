﻿namespace NewsBeautifier.Views
{
	using Base;

	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainView : WindowsPhoneBasePage
	{
		public MainView()
		{
			this.InitializeComponent();
		}
	}
}
