﻿namespace NewsBeautifier.Core.DocumentResponse
{
	using Newtonsoft.Json;

	public class Feed
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("icon")]
		public string Icon { get; set; }
		[JsonProperty("url")]
		public string Url { get; set; }
		[JsonProperty("categoryid")]
		public string CategoryId { get; set; }
		[JsonProperty("posts")]
		public Post[] Posts { get; set; }
	}
}
