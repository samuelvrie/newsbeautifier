﻿namespace NewsBeautifier.Core.ViewModels
{
	using System;
	using System.Globalization;
	using System.Linq;
	using NewsBeautifier.Core.DocumentResponse;
	using NewsBeautifier.Core.Settings;
	using NewsBeautifier.Core.ViewModels.Base;
	using Windows.UI.Popups;

	public class PostDetailViewModel : NavegableViewModel
	{
		private string _title;
		private string _content;

		public string Content 
		{
			get { return _content; }
			set
			{
				_content = value;
				RaisePropertyChanged(() => Content);
			}
		}
		public string Title 
		{
			get { return _title; }
			set
			{
				_title = value;
				RaisePropertyChanged(() => Title);
			}
		}

		public PostDetailViewModel()
		{
			InitializeCommands();
		}

		public async void Init(string currentPostId)
		{
			string error = string.Empty;

			try
			{
				var postTable = CommonAppSettings.MobileService.GetTable<Post>();
				var post = await postTable
					.Where(p => p.Id == currentPostId)
					.Select(p => new { Title = p.Title, Content = p.Content })
					.ToEnumerableAsync();

				Content = string.Format(CultureInfo.InvariantCulture, @"<!DOCTYPE html><html><body>{0}</body></html>", post.Single().Content);
				Title = post.Single().Title;
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			if (!string.IsNullOrEmpty(error))
			{
				await new MessageDialog(error, "Une erreur est survenue").ShowAsync();
				Close(this);
			}
		}

		private void InitializeCommands()
		{
		}
	}
}
