namespace NewsBeautifier
{
	using Cirrious.CrossCore.Platform;
	using Cirrious.MvvmCross.ViewModels;
	using Cirrious.MvvmCross.WindowsCommon.Platform;
	using Windows.UI.Xaml.Controls;

	public class Setup : MvxWindowsSetup
    {
		protected override void InitializeFirstChance()
		{
			base.InitializeFirstChance();
		}

        public Setup(Frame rootFrame) : base(rootFrame)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }
		
        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }
    }
}