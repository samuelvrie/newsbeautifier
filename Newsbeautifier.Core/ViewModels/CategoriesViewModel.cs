﻿namespace NewsBeautifier.Core.ViewModels
{
	using System;
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using DocumentResponse;
	using Interfaces;
	using Microsoft.WindowsAzure.MobileServices;
	using NewsBeautifier.Core.Settings;
	using Windows.UI.Popups;

	public class CategoriesViewModel : MvxViewModel, ICategoriesViewModel
	{
		private MobileServiceCollection<Category, Category> _categories;
		private IMobileServiceTable<Category> _categoriesTable = CommonAppSettings.MobileService.GetTable<Category>();
		private bool _isLoadingCategories;
		private ICommand _navigateToFeedDetailCommand;

		public CategoriesViewModel()
		{
			InitializeActions();
			InitializeCommands();
		}

		private void InitializeCommands()
		{
			_navigateToFeedDetailCommand = new MvxCommand<string>(NavigateToFeedDetails);
		}

		private void NavigateToFeedDetails(string currentFeedId)
		{
			ShowViewModel<PostsViewModel>(new { currentFeedId = currentFeedId});
		}

		public MobileServiceCollection<Category, Category> Categories
		{
			get
			{
				return _categories;
			}
			private set
			{
				_categories = value;
				RaisePropertyChanged(() => Categories);
			}
		}

		public bool IsLoadingCategories
		{
			get
			{
				return _isLoadingCategories;
			}
			private set
			{
				_isLoadingCategories = value;
				RaisePropertyChanged(() => IsLoadingCategories);
			}
		}
		public ICommand NavigateToFeedDetailCommand
		{
			get { return _navigateToFeedDetailCommand; }
		}

		private async void InitializeActions()
		{
			string error = string.Empty;
			IsLoadingCategories = true;

			try
			{
				Categories = await _categoriesTable.ToCollectionAsync();
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}
			if (!string.IsNullOrEmpty(error))
			{
				await new MessageDialog(error, "Une erreur est survenue").ShowAsync();
			}
			IsLoadingCategories = false;
		}

	}
}
