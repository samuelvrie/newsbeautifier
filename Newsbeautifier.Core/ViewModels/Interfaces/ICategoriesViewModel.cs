﻿namespace NewsBeautifier.Core.ViewModels.Interfaces
{
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using Microsoft.WindowsAzure.MobileServices;
	using NewsBeautifier.Core.DocumentResponse;

	public interface ICategoriesViewModel : IMvxViewModel
	{
		ICommand NavigateToFeedDetailCommand { get; }
		bool IsLoadingCategories { get; }
		MobileServiceCollection<Category, Category> Categories { get; }
	}
}
