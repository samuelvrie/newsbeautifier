﻿namespace NewsBeautifier.Core.ViewModels
{
	using System;
	using System.Collections.ObjectModel;
	using System.Linq;
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using Microsoft.WindowsAzure.MobileServices;
	using NewsBeautifier.Core.DocumentResponse;
	using NewsBeautifier.Core.Settings;
	using NewsBeautifier.Core.ViewModels.Base;
	using NewsBeautifier.Core.ViewModels.Interfaces;
	using Windows.UI.Popups;

	public class PostsViewModel : NavegableViewModel, IPostsViewModel
	{
		ObservableCollection<Post> _posts;
		ICommand _navigateToPostDetailCommand;
		bool _isLoadingPosts;

		public PostsViewModel()
		{
			InitializeCommands();
		}

		public ObservableCollection<Post> Posts
		{
			get { return _posts; }
			private set
			{
				_posts = value;
				RaisePropertyChanged(() => Posts);
			}
		}
		public bool IsLoadingPosts
		{
			get { return _isLoadingPosts; }
			private set 
			{ 
				_isLoadingPosts = value; 
				RaisePropertyChanged(() => IsLoadingPosts); 
			}
		}

		public ICommand NavigateToPostDetailCommand
		{
			get { return _navigateToPostDetailCommand; }
		}

		private void InitializeCommands()
		{
			_navigateToPostDetailCommand = new MvxCommand<string>(NavigateToPost);
		}

		private void NavigateToPost(string currentPostId)
		{
			ShowViewModel<PostDetailViewModel>(new { currentPostId = currentPostId});
		}


		public async void Init(string currentFeedId)
		{
			MobileServiceClient client = CommonAppSettings.MobileService;
			string error = string.Empty;

			IsLoadingPosts = true;
			try
			{
				var feedTable = client.GetTable<Feed>();
				var feed = await feedTable.Where(c => c.Id == currentFeedId).ToEnumerableAsync();
				Posts = new ObservableCollection<Post>(feed.First().Posts);
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}

			if (!string.IsNullOrEmpty(error))
			{
				var messageDialog = new MessageDialog(error, "Une erreur est survenue");
				Close(this);
			}
			IsLoadingPosts = false;
		}
	}
}
