﻿namespace NewsBeautifier.Core.ViewModels.Base
{
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using NewsBeautifier.Core.ViewModels.Behaviors;

	public class NavegableViewModel :
		MvxViewModel,
		ICanGoBackViewModel
	{
		private ICommand _goBackCommand;

		public NavegableViewModel()
		{
			InitializeCommands();
		}

		public ICommand GoBackCommand { get { return _goBackCommand; } }

		private void InitializeCommands()
		{
			_goBackCommand = new MvxCommand(GoBack);
		}

		private void GoBack()
		{
			Close(this);
		}
	}
}
