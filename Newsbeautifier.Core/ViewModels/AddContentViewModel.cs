﻿namespace NewsBeautifier.Core.ViewModels
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Linq;
	using System.Threading.Tasks;
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using Microsoft.WindowsAzure.MobileServices;
	using NewsBeautifier.Core.DocumentResponse;
	using NewsBeautifier.Core.Settings;
	using NewsBeautifier.Core.ViewModels.Base;
	using Windows.UI.Popups;
	using Windows.Web.Syndication;

	public class AddContentViewModel : NavegableViewModel
	{
		ICommand _searchCommand;
		ICommand _addFeedCommand;
		private string _feedUrl;
		private string _feedTitle;
		private string _feedSubTitle;
		private string _categoryName;
		private string _categoryId;
		private Uri _imageUri;
		private bool _isGridVisibile;

		MobileServiceClient serviceClient = CommonAppSettings.MobileService;
		private Task<IEnumerable<Post>> _tempPost;

		public AddContentViewModel()
		{
			InitiazeActions();
			InitializeCommands();
		}

		public string FeedUrl
		{
			get { return _feedUrl; }
			set
			{
				_feedUrl = value;
			}
		}
		public string FeedTitle
		{
			get { return _feedTitle; }
			set
			{
				_feedTitle = value;
				RaisePropertyChanged(() => FeedTitle);
			}
		}
		public string FeedSubTitle
		{
			get
			{
				return _feedSubTitle;
			}
			set
			{
				_feedSubTitle = value;
				RaisePropertyChanged(() => FeedSubTitle);
			}
		}
		public string CategoryName
		{
			get { return _categoryName; }
			set
			{
				_categoryName = value;
				RaisePropertyChanged(() => CategoryName);
			}
		}
		public string CategoryId
		{
			get { return _categoryId; }
			private set { _categoryId = value; }
		}
		public Uri ImageUri
		{
			get
			{
				return _imageUri;
			}
			set
			{
				_imageUri = value;
				RaisePropertyChanged(() => ImageUri);
			}
		}
		public bool IsGridVisible
		{
			get { return _isGridVisibile; }
			set
			{
				_isGridVisibile = value;
				RaisePropertyChanged(() => IsGridVisible);
			}
		}
		public ICommand SearchCommand { get { return _searchCommand; } }
		public ICommand AddFeedCommand { get { return _addFeedCommand; } }

		private void InitiazeActions()
		{
			IsGridVisible = false;
			// Recherche des category associe
		}

		private async void SearchFeed()
		{
			string error = string.Empty;

			FeedTitle = string.Empty;
			FeedSubTitle = string.Empty;
			ImageUri = null;
			Uri uri;

			if (Uri.TryCreate(_feedUrl, UriKind.Absolute, out uri))
			{
				try
				{
					var client = new SyndicationClient();
					var data = await client.RetrieveFeedAsync(uri);

					FeedTitle = data.Title != null ? data.Title.Text : "No title";
					FeedSubTitle = data.Subtitle != null ? data.Subtitle.Text : "No subtitle";
					// Date de publication, Title, Content(HTML), Authors
					ImageUri = data.IconUri;
					_tempPost = Task.Run(() =>
						data.Items.Select(post => new Post
						{
							PostId = post.Id,
							Content = post.Content.Text,
							Title = post.Title.Text,
							PublishedData = post.PublishedDate.Date
						}));
					IsGridVisible = true;
				}
				catch (Exception ex)
				{
					if (SyndicationError.GetStatus(ex.HResult) == SyndicationErrorStatus.InvalidXml)
						error = "Cette url n'est pas de type RSS ou Atom.";
					error = ex.Message;
				}
			}
			else
				error = "Url non valide";
			if (!string.IsNullOrEmpty(error))
			{
				await new MessageDialog(string.Format(CultureInfo.CurrentCulture, "{0}", error), "Erreur dans l'url").ShowAsync();
			}
		}

		private async void AddFeed()
		{
			string error = string.Empty;

			if (string.IsNullOrEmpty(FeedUrl) || string.IsNullOrEmpty(CategoryName))
			{
				await new MessageDialog("Tous les champs doivent etre remplit").ShowAsync();
				return;
			}
			try
			{
				MobileServiceClient client = CommonAppSettings.MobileService;

				var categoryTable = client.GetTable<Category>();
				var feed = new Feed { Id = Guid.NewGuid().ToString(), Url = FeedUrl, Name = FeedTitle };
				feed.Posts = (await _tempPost).ToArray();
				feed.Icon = ImageUri != null ? ImageUri.AbsoluteUri : null;

				var category = await categoryTable.Where(x => x.Name == CategoryName).ToEnumerableAsync();

				if (category.Count() > 0)
				{
					var feedTable = client.GetTable<Feed>();
					feed.CategoryId = category.First().Id;
					await feedTable.InsertAsync(feed);
				}
				else
				{
					var payload = new Category
						{
							Id = Guid.NewGuid().ToString(),
							Name = CategoryName,
							Feeds = new Feed[] { feed }
						};
					feed.CategoryId = payload.Id;
					await categoryTable.InsertAsync(payload);
				}
				var postTable = client.GetTable<Post>();
				foreach (var item in feed.Posts)
				{
					item.FeedId = feed.Id; 
					await postTable.InsertAsync(item);
				}
				Close(this);
			}
			catch (Exception ex)
			{
				error = ex.Message;
			}
		}

		private void InitializeCommands()
		{
			_searchCommand = new MvxCommand(() => SearchFeed());
			_addFeedCommand = new MvxCommand(() => AddFeed());
		}
	}
}
