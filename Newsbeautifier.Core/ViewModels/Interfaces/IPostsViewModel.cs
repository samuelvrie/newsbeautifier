﻿namespace NewsBeautifier.Core.ViewModels.Interfaces
{
	using System.Collections.ObjectModel;
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using NewsBeautifier.Core.DocumentResponse;

	public interface IPostsViewModel : IMvxViewModel
	{
		bool IsLoadingPosts { get; }
		ICommand NavigateToPostDetailCommand { get; }
		ObservableCollection<Post> Posts { get; }
	}
}
