﻿namespace NewsBeautifier.Views
{
	using NewsBeautifier.Views.Base;

	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class AddContentView : WindowsPhoneBasePage
	{
		public AddContentView()
		{
			this.InitializeComponent();
		}
	}
}
