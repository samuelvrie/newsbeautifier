﻿namespace NewsBeautifier.Core.ViewModels
{
	using System.Windows.Input;
	using Cirrious.MvvmCross.ViewModels;
	using NewsBeautifier.Core.ViewModels.Interfaces;

	public class MainViewModel : MvxViewModel
	{
		private ICommand _navigateToSettingsCommand;
		private ICommand _navigateToAddContentCommand;

		public ICategoriesViewModel CategoriesViewModel { get; private set; }

		public MainViewModel(
			ICategoriesViewModel categoriesViewModel)
		{
			CategoriesViewModel = categoriesViewModel;
			// Saved post
			// New Post - Should Be first 

			InitializeCommands();
		}

		public ICommand NavigateToAddContentCommand { get { return _navigateToAddContentCommand; } }
		public ICommand NavigateToSettingsCommand { get { return _navigateToSettingsCommand; } }

		private void InitializeCommands()
		{
			_navigateToAddContentCommand = new MvxCommand(() => ShowViewModel<AddContentViewModel>());
			_navigateToSettingsCommand = new MvxCommand(() => new Windows.UI.Popups.MessageDialog("Need to be implemented").ShowAsync()); // Rajouter une view de settings avec possibilite de Logout
		}
	}
}
