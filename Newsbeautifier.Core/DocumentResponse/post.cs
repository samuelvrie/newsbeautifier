﻿namespace NewsBeautifier.Core.DocumentResponse
{
	using System;
	using Newtonsoft.Json;

	public class Post
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("title")]
		public string Title { get; set; }
		[JsonProperty("content")]
		public string Content { get; set; }
		[JsonProperty("publishedate")]
		public DateTime PublishedData { get; set; }

		[JsonProperty("postid")]
		public string PostId { get; set; }
		[JsonProperty("feedid")]
		public string FeedId { get; set; }
	}
}
