﻿namespace NewsBeautifier.Converters
{
	using System;
	using Windows.UI.Xaml;
	using Windows.UI.Xaml.Data;
	
	public class BoolToVisibiltyConverter : IValueConverter
    {
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			return System.Convert.ToBoolean(value)
				? Visibility.Visible 
				: Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			return ((Visibility)value) == Visibility.Visible 
				? true 
				: false; 
		}
	}
}
