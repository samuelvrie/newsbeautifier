﻿namespace NewsBeautifier.Core.DocumentResponse
{
	using Newtonsoft.Json;

	public class Category
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("userid")]
		public string UserId { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("feeds")]
		public Feed[] Feeds { get; set; }
	}



}
