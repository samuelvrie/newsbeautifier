﻿namespace NewsBeautifier.Core.Settings
{
	using Microsoft.WindowsAzure.MobileServices;

	public static class CommonAppSettings
	{
		//private readonly static string _MobileServiceUrl = "http://localhost:59474";
		private readonly static string _MobileServiceUrl = "https://newsbeautifier.azure-mobile.net/";
		private readonly static string _MobileServiceKey = "eNgdjxPemnFonxzsXGphoLGFarJRMu55";

		private static MobileServiceClient mobileService = null;

		public static string MobileServiceUrl
		{
			get
			{
				return _MobileServiceUrl;
			}
		}
		public static string MobileServiceKey
		{
			get
			{
				return _MobileServiceKey;
			}
		}

		public static MobileServiceClient MobileService
		{
			get
			{
				return mobileService ?? (mobileService = new MobileServiceClient(MobileServiceUrl, MobileServiceKey));
			}
		}

		//public static MobileServiceClient MobileService
		//{
		//	get
		//	{
		//		return mobileService ?? (mobileService = new MobileServiceClient(MobileServiceUrl));
		//	}
		//}
	}
}
