﻿namespace NewsBeautifier.Core.ViewModels.Behaviors
{
	using System.Windows.Input;

	public interface ICanGoBackViewModel
	{
		ICommand GoBackCommand { get; }
	}
}
