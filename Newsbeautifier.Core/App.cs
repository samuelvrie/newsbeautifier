namespace NewsBeautifier.Core
{
	using Cirrious.CrossCore;
	using Cirrious.CrossCore.IoC;
	using Cirrious.MvvmCross.ViewModels;
	using NewsBeautifier.Core.ViewModels;
	using NewsBeautifier.Core.ViewModels.Interfaces;

	public class App : MvxApplication
    {
        public override void Initialize()
        {
			Mvx.RegisterType<ICategoriesViewModel, CategoriesViewModel>();

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
				
            RegisterAppStart<MainViewModel>();
        }
    }
}